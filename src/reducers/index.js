import { combineReducers } from 'redux';
import procesosReducer from './procesosReducer';
import alertaReducer from './alertaReducer';



export default combineReducers({
    tareas: procesosReducer,
    alerta: alertaReducer
});