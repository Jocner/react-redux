import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { editarTareaAction } from '../actions/procesosActions';
import { useHistory } from 'react-router-dom';

const EditarTarea = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    // nuevo state de producto
    const [ tarea, guardarTarea] = useState({
        nombre: '',
        descripcion: '',
        fechacreacion: '',
        estado: '' 
    })

    // producto a editar
    const tareaeditar = useSelector(state => state.tareas.tareaeditar);
  
    // llenar el state automaticamente
    useEffect( () => {
        guardarTarea(tareaeditar);
    }, [tareaeditar]);

    // Leer los datos del formulario
    const onChangeFormulario = e => {
        guardarTarea({
            ...tarea,
            [e.target.name] : e.target.value
        })
    }


    const { nombre, descripcion, fechacreacion, estado} = tarea;

    const submitEditarTarea = e => {
        e.preventDefault();

        dispatch( editarTareaAction(tarea) );
    
        history.push('/');
    }
    
    return ( 
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Editar Tarea
                        </h2>

                        <form
                            onSubmit={submitEditarTarea}
                        >
                            <div className="form-group">
                                <label>Tarea</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre de la Tarea"
                                    name="nombre"
                                    value={nombre}
                                    onChange={onChangeFormulario}
                                />
                            </div>

                            <div className="form-group">
                                <label>Descripcion</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Descripcion Tarea"
                                    name="descripcion"
                                    value={descripcion}
                                    onChange={onChangeFormulario}
                                />
                            </div>

                            <div className="form-group">
                                <label>Fecha de Modificacion</label>
                                <input
                                    type="date"
                                    className="form-control"
                                    placeholder="Fecha Modificacion"
                                    name="fechacreacion"
                                    value={fechacreacion}
                                    onChange={onChangeFormulario}
                                />
                            </div>

                            <div className="form-group">
                                <label>Estado</label>
                                <input
                                    type="date"
                                    className="form-control"
                                    placeholder="Estado"
                                    name="estado"
                                    value={estado}
                                    onChange={onChangeFormulario}
                                />
                            </div>

                            <button 
                                type="submit"
                                className="btn btn-primary font-weight-bold text-uppercase d-block w-100"
                            >Guardar Cambios</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
     );
}
 
export default EditarTarea;